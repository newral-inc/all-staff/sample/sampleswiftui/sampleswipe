//
//  ContentView.swift
//  SampleSwipe
//
//  Created by Masatsugu Futamata on 2022/01/22.
//
// 以下を使用した
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            ItemList()
        }
    }
}

struct ItemList: View {
    private let items = ["apple", "banana", "orange"]
    @State private var selection: String?
    var body: some View {
        List(selection: $selection) {
            ForEach(items, id: \.self) { item in
                NavigationLink(destination: ItemList()) {
                    Text(item)
                }
                .swipeActions() {
                    Button() {
                        print("スワイプ \(item)")
                    } label: {
                        Image(systemName: "menucard")
                    }
                    .tint(.orange)
                }
                .onTapGesture {
                    print("タップ \(item)")
                }
                .onLongPressGesture() {
                    print("ロングプレス \(item)")
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

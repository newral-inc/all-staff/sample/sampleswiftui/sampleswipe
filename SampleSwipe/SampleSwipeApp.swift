//
//  SampleSwipeApp.swift
//  SampleSwipe
//
//  Created by Masatsugu Futamata on 2022/01/22.
//

import SwiftUI

@main
struct SampleSwipeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
